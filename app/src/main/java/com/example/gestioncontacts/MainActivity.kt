package com.example.gestioncontacts

import android.content.Context
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.EditText
import android.widget.ListView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
//import android.support.v7.app.AlertDialog
import android.widget.*
import androidx.core.content.ContextCompat.startActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.row_main.*

const val EXTRA_MESSAGE = "com.example.GestionContacts.MESSAGE"


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val listView= findViewById<ListView>(R.id.main_listview)
        //val redcolor= Color.parseColor("#FF0000")
        //listView.setBackgroundColor(redcolor)

        listView.adapter= MyCustomAdapter(this)

        /*button.setOnClickListener {
            val intent = Intent(this@MainActivity, FicheContact::class.java)
            startActivity(intent)
        }*/

    }

    public  class MyCustomAdapter(context: Context): BaseAdapter(){
        public  val mcontext: Context
        public  val names= arrayListOf<String>(
            "Mohamed Cissé", "Medoune Ba", "Ousmane Coulibaly", "Birame Ba"
        )
        init {
            mcontext= context
        }
        override fun getCount(): Int {
            return names.size
        }
        override fun getItemId(position: Int): Long {
           return position.toLong()
        }
        override fun getItem(position: Int): Any {
            return "Test Listview"
        }
        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val layoutInflater= LayoutInflater.from(mcontext)
            val rowMain=layoutInflater.inflate(R.layout.row_main, parent,false)
            val  nameTextView= rowMain.findViewById<TextView>(R.id.textView)
            nameTextView.text= names.get(position)
            val positionTextView= rowMain.findViewById<TextView>(R.id.position_textview)
            positionTextView.text= "Row Number: $position"

            // Set a click listener for button widget
            val button2 = rowMain.findViewById<Button>(R.id.button2)
            button2.setOnClickListener{
                // Initialize a new instance of
                val builder = AlertDialog.Builder(mcontext)

                // Set the alert dialog title
                builder.setTitle("Suppression de contact")

                // Display a message on alert dialog
                builder.setMessage("Voulez-vous supprimer ce contact?")

                // Set a positive button and its click listener on alert dialog
                builder.setPositiveButton("YES"){dialog, which ->
                    // Do something when user press the positive button
                    Toast.makeText(mcontext,"Ok, we change the app background.",Toast.LENGTH_SHORT).show()

                    // Change the app background color
                    //root_layout.setBackgroundColor(Color.RED)
                }


                // Display a negative button on alert dialog
                builder.setNegativeButton("No"){dialog,which ->
                    Toast.makeText(mcontext,"You are not agree.",Toast.LENGTH_SHORT).show()
                }


                // Display a neutral button on alert dialog
                builder.setNeutralButton("Cancel"){_,_ ->
                    Toast.makeText(mcontext,"You cancelled the dialog.",Toast.LENGTH_SHORT).show()
                }

                // Finally, make the alert dialog using builder
                val dialog: AlertDialog = builder.create()

                // Display the alert dialog on app interface
                dialog.show()
            }

            /** Called when the user taps the Send button */
            val button = rowMain.findViewById<Button>(R.id.button)
            button.setOnClickListener {
                //fun sendMessage(view: View) {
                //val textView = rowMain.findViewById<TextView>(R.id.textView)
                val message = nameTextView.text.toString()
                val intent = Intent(mcontext, FicheContact::class.java).apply {
                    putExtra(EXTRA_MESSAGE, message)
                }
                mcontext.startActivity(intent)

                //}
            }


            return  rowMain
            //val  textView= TextView(mcontext)
            //textView.text= "Voici ma listview"
            //return textView
        }
    }
}
